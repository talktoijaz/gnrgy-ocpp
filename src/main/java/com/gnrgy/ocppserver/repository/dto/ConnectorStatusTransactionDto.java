package com.gnrgy.ocppserver.repository.dto;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConnectorStatusTransactionDto {

    private final String chargeBoxId, timeStamp, status, errorCode;
    private final int chargeBoxPk, connectorId;
    private final DateTime statusTimestamp;
    private final Integer transactionId;
    
    private final String label;//connector_detail.label
    private final String barCode;
    
    //private final String connectorLabel; //connector_detail.label
    private final BigDecimal maxPower; //connectorType.max_power
    //private final String maxPowerString; //connectorType.max_power in kW string 
    private final String connectorTypeStandard; //connectorType.connectorType
    private final String plugType; //connectorType.shortDescription
    //private final String connectorChargingFacilityType; //connectorType.powerType + connectorType.max_power in string
    //private final String connectorTypeChargingModType;//connectorType.connectorType
    private final String connectorChargePointType;//connectorType.powerType
    
    
    
    
    
}
