package com.gnrgy.ocppserver.repository.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChargeBoxSiteDto {

	private String charge_box_id;
	private Integer site_id;
	private Integer customer_id;
	private Integer id_tag_group_id;
	private BigDecimal lattitude;
	private BigDecimal longitude;
	private String street;
	private String city;
	private String country;
	private Boolean remoteStart;
	private String model;
	private String vendor;
      
}
