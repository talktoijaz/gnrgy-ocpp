package com.gnrgy.ocppserver.repository.impl;

import static jooq.steve.db.tables.Connector.CONNECTOR;
import static jooq.steve.db.tables.Transaction.TRANSACTION;
import static jooq.steve.db.tables.ChargeBox.CHARGE_BOX;
import static jooq.steve.db.tables.ChargeBoxSite.CHARGE_BOX_SITE;
import static jooq.steve.db.tables.ConnectorsDetail.CONNECTORS_DETAIL;
import static jooq.steve.db.tables.ConnectorTypes.CONNECTOR_TYPES;

import java.util.List;

import org.joda.time.DateTime;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gnrgy.ocppserver.repository.CustomTransactionRepository;
import com.gnrgy.ocppserver.repository.dto.OngoingSessionsDto;

import jooq.steve.db.tables.ChargeBox;
import jooq.steve.db.tables.ConnectorTypes;

@Repository
public class CustomTransactionRepositoryImpl implements CustomTransactionRepository{

	private final DSLContext ctx;
	
	@Autowired
    public CustomTransactionRepositoryImpl(DSLContext ctx) {
        this.ctx = ctx;
    }
	
	@Override
	public List<Integer> getActiveTransactionIds(String chargeBoxId, Integer connectorId, String idTag) {
		return ctx.select(TRANSACTION.TRANSACTION_PK)
                .from(TRANSACTION)
                .join(CONNECTOR)
                  .on(TRANSACTION.CONNECTOR_PK.equal(CONNECTOR.CONNECTOR_PK))
                  .and(CONNECTOR.CONNECTOR_ID.equal(connectorId))
                  .and(CONNECTOR.CHARGE_BOX_ID.equal(chargeBoxId))
                .where(TRANSACTION.STOP_TIMESTAMP.isNull())
                .and(TRANSACTION.ID_TAG.equal(idTag))
                .and(TRANSACTION.START_EVENT_TIMESTAMP.between(new DateTime().minusMinutes(3), new DateTime()))
                .fetch(TRANSACTION.TRANSACTION_PK);
	}
	
	@Override
	public Integer getActiveTransactionIdsByTransactionId(String chargeBoxId, Integer connectorId,Integer transactionId) {
		return ctx.select(TRANSACTION.TRANSACTION_PK)
                .from(TRANSACTION)
                .join(CONNECTOR)
                  .on(TRANSACTION.CONNECTOR_PK.equal(CONNECTOR.CONNECTOR_PK))
                  .and(CONNECTOR.CONNECTOR_ID.equal(connectorId))
                  .and(CONNECTOR.CHARGE_BOX_ID.equal(chargeBoxId))
                .where(TRANSACTION.STOP_TIMESTAMP.isNull())
                .and(TRANSACTION.TRANSACTION_PK.eq(transactionId))
                .fetchOne(TRANSACTION.TRANSACTION_PK);
	}
	
	@Override
	public List<OngoingSessionsDto> getOngoingSessions(List<String> idTags) {
		final Condition idTagsCondition =TRANSACTION.ID_TAG.in(idTags);
		
		
		 //Fetch max transaction Id
        Field<Integer> connectorPk = TRANSACTION.CONNECTOR_PK.as("connectorPk");
        Field<Integer> transactionPkMax = DSL.max(TRANSACTION.TRANSACTION_PK).as("transactionPkMax");
        Table<?> transactionMax = ctx.select(connectorPk, transactionPkMax)
                         .from(TRANSACTION)
                         .where(TRANSACTION.STOP_TIMESTAMP.isNull())
                         .and(idTagsCondition)
                         .groupBy(TRANSACTION.CONNECTOR_PK)
                         .asTable("transactionMax");
		
		
		
		
		return ctx.select(transactionMax.field(transactionPkMax),
				TRANSACTION.START_EVENT_TIMESTAMP,
				TRANSACTION.ID_TAG,
				CHARGE_BOX.CHARGE_BOX_ID,
				CHARGE_BOX_SITE.SITE_ID,
				CONNECTOR.CONNECTOR_ID,
				CONNECTOR_TYPES.CONNECTOR_TYPE,
				CONNECTORS_DETAIL.MAX_POWER,
				CONNECTORS_DETAIL.LABEL
				)
				.from(TRANSACTION)
				.join(transactionMax)
					.on(TRANSACTION.TRANSACTION_PK.eq(transactionMax.field(transactionPkMax)))
				.join(CONNECTOR)
					.on(CONNECTOR.CONNECTOR_PK.eq(TRANSACTION.CONNECTOR_PK))
				.join(CHARGE_BOX)
					.on(CHARGE_BOX.CHARGE_BOX_ID.eq(CONNECTOR.CHARGE_BOX_ID))
				.leftJoin(CONNECTORS_DETAIL)
					.on(CONNECTORS_DETAIL.CONNECTOR_ID.eq(CONNECTOR.CONNECTOR_ID))
					.and(CONNECTORS_DETAIL.CHARGE_BOX_ID.eq(CONNECTOR.CHARGE_BOX_ID))
				.leftJoin(CONNECTOR_TYPES)
					.on(CONNECTOR_TYPES.ID.eq(CONNECTORS_DETAIL.CONNECTOR_TYPE_ID))
				.leftJoin(CHARGE_BOX_SITE)
					.on(CHARGE_BOX_SITE.CHARGE_BOX_ID.eq(CHARGE_BOX.CHARGE_BOX_ID))
				.where(idTagsCondition)
				.and(TRANSACTION.STOP_TIMESTAMP.isNull())
				.fetch()
				.map(r -> OngoingSessionsDto.builder()
						.transactionId(r.value1())
						.startTime(r.value2())
						.idTag(r.value3())
						.uuid(r.value4())
						.siteId(r.value5())
						.connectorUuid(r.value6())
						.connectorType(r.value7())
						.maxPower(r.value8())
						.connectorLabel(r.value9())
						.build()
						);
	}
	

}
