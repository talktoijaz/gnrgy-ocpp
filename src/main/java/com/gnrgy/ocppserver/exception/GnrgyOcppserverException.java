package com.gnrgy.ocppserver.exception;

import static java.lang.String.format;

import org.springframework.http.HttpStatus;

public class GnrgyOcppserverException extends RuntimeException {

	private HttpStatus httpStatus;
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	public GnrgyOcppserverException(String errorCode) {
        super(errorCode);
    }
	*/
	
	public GnrgyOcppserverException(String errorCode,HttpStatus httpStatus) {
        super(errorCode);
        this.httpStatus = httpStatus;
    }

    public GnrgyOcppserverException(String errorCode, HttpStatus httpStatus,Throwable cause) {
        super(errorCode, cause);
        this.httpStatus = httpStatus;
    }

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
    
    
    
}
