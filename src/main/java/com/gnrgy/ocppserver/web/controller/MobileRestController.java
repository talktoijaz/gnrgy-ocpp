package com.gnrgy.ocppserver.web.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.service.CustomChargePointHelperService;
import com.gnrgy.ocppserver.service.CustomOcppTagService;
import com.gnrgy.ocppserver.service.CustomTransactionService;
import com.gnrgy.ocppserver.web.dto.ConnectorUuid;
import com.gnrgy.ocppserver.web.dto.request.PingRequest;
import com.gnrgy.ocppserver.web.dto.request.RemoteStartRequest;
import com.gnrgy.ocppserver.web.dto.request.RemoteStopRequest;
import com.gnrgy.ocppserver.web.dto.request.ReservationRequest;
import com.gnrgy.ocppserver.web.dto.response.PingResponse;
import com.gnrgy.ocppserver.web.dto.response.RemoteStartResponse;
import com.gnrgy.ocppserver.web.dto.response.RemoteStopResponse;
import com.gnrgy.ocppserver.web.dto.response.ReservationResponse;
import com.gnrgy.ocppserver.web.util.Constant;

import de.rwth.idsg.steve.ocpp.CommunicationTask;
import de.rwth.idsg.steve.ocpp.RequestResult;
import de.rwth.idsg.steve.ocpp.task.ReserveNowTask;
import de.rwth.idsg.steve.repository.ChargePointRepository;
import de.rwth.idsg.steve.repository.TaskStore;
import de.rwth.idsg.steve.service.ChargePointHelperService;
import de.rwth.idsg.steve.service.ChargePointService16_Client;
import de.rwth.idsg.steve.service.OcppTagService;
import de.rwth.idsg.steve.web.dto.ocpp.RemoteStartTransactionParams;
import de.rwth.idsg.steve.web.dto.ocpp.RemoteStopTransactionParams;
import de.rwth.idsg.steve.web.dto.ocpp.ReserveNowParams;
import jooq.steve.db.tables.records.ChargeBoxRecord;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;
import jooq.steve.db.tables.records.OcppTagDetailRecord;
import lombok.extern.slf4j.Slf4j;
import ocpp.cs._2015._10.AuthorizationStatus;
import ocpp.cs._2015._10.IdTagInfo;

@Slf4j
@Controller
@RequestMapping(value = "/mobile")
public class MobileRestController {

	private static final String REMOTE_START = "/remote_start";
	private static final String REMOTE_STOP = "/remote_stop";
	private static final String PING = "/ping";
	private static final String RESERVATION = "/reservation";

	@Autowired
	@Qualifier("ChargePointService16_Client")
	private ChargePointService16_Client client16;

	@Autowired
	private TaskStore taskStore;

	@Autowired
	private CustomTransactionService customTransactionService;

	@Autowired
	protected ChargePointHelperService chargePointHelperService;

	@Autowired
	private ChargePointRepository chargePointRepository;

	@Autowired
	private CustomChargePointHelperService customChargePointHelperService;
	
	@Autowired
	private OcppTagService ocppTagService;
	
	@Autowired
	private CustomOcppTagService customOcppTagService;

	protected ChargePointService16_Client getClient16() {
		return client16;
	}

	@RequestMapping(value = REMOTE_START, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> remoteStart(@RequestBody RemoteStartRequest remoteStartRequest,
			HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), remoteStartRequest.toString());
			ConnectorUuid connectorUuid = new ConnectorUuid(remoteStartRequest.getConnector());
			checkAuthentication(connectorUuid.getChargeBoxId(),remoteStartRequest.getIdentifier());
			RemoteStartTransactionParams remoteStartTransactionParams = new RemoteStartTransactionParams();
			remoteStartTransactionParams.setIdTag(remoteStartRequest.getIdentifier());
			remoteStartTransactionParams.setConnectorId(connectorUuid.getConnectorId());
			remoteStartTransactionParams.setChargePointSelectList(
					customChargePointHelperService.getchargePointSelectList(connectorUuid.getChargeBoxId()));
			int taskId = getClient16().remoteStartTransaction(remoteStartTransactionParams);

			CommunicationTask r = customChargePointHelperService.checkTaskFinished(taskId);
			RemoteStartResponse remoteStartResponse = populateRemoteStartTransaction(r, connectorUuid,
					remoteStartRequest.getIdentifier());
			log.info("Response: {} ", remoteStartResponse);
			return new ResponseEntity<>(remoteStartResponse, HttpStatus.OK);
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	@RequestMapping(value = REMOTE_STOP, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> remoteStop(@RequestBody RemoteStopRequest remoteStopRequest,
			HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), remoteStopRequest.toString());
			RemoteStopTransactionParams remoteStartTransactionParams = new RemoteStopTransactionParams();
			ConnectorUuid connectorUuid = new ConnectorUuid(remoteStopRequest.getConnector());
			Integer activeTransaction = customTransactionService.getActiveTransactionIdsByTransactionId(
					connectorUuid.getChargeBoxId(), connectorUuid.getConnectorId(), remoteStopRequest.getTransaction());
			if (activeTransaction != null) {
				remoteStartTransactionParams.setChargePointSelectList(
						customChargePointHelperService.getchargePointSelectList(connectorUuid.getChargeBoxId()));
				remoteStartTransactionParams.setTransactionId(remoteStopRequest.getTransaction());
				int taskId = getClient16().remoteStopTransaction(remoteStartTransactionParams);
				CommunicationTask r = customChargePointHelperService.checkTaskFinished(taskId);
				RemoteStopResponse remoteStopResponse = populateRemoteStopTransaction(r, connectorUuid);
				log.info("Response: {} ", remoteStopResponse);
				return new ResponseEntity<>(remoteStopResponse, HttpStatus.OK);
			} else {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_TRANSACTION_STARTED,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}


	@RequestMapping(value = PING, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> ping(@RequestBody PingRequest pingRequest,
			@RequestParam(required = false) Boolean ignore_portal_configuration, HttpServletRequest request)
			throws Exception {
		try {
			PingResponse pingResponse = new PingResponse();
			String ipAddress = null;
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), pingRequest.toString());
			if (pingRequest.getSerialNumber() != null) {
				ChargeBoxRecord chargeBoxRecord = customChargePointHelperService
						.getChargeBox(pingRequest.getSerialNumber());
				if (chargeBoxRecord == null) {
					throw new GnrgyOcppserverException(Constant.ERROR_CODE_CB_NOT_FOUND,
							HttpStatus.INTERNAL_SERVER_ERROR);
				}
				ipAddress = chargeBoxRecord.getIpAddress();
			} else if (pingRequest.getIpAddress() != null) {
				ipAddress = pingRequest.getIpAddress();
			} else {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_PARAM_MISSING, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			pingResponse = populatePingResponse(ipAddress);
			log.info("Response: {} ", pingResponse);
			return new ResponseEntity<>(pingResponse, HttpStatus.OK);
		} catch (GnrgyOcppserverException e) {
			throw new GnrgyOcppserverException(e.getMessage(), e.getHttpStatus(), e);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	@RequestMapping(value = RESERVATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> reservation(@RequestBody ReservationRequest reservationRequest,
			HttpServletRequest request) throws Exception {
		try {
			log.info("Request: {} raised with parameters {}", request.getRequestURL(), reservationRequest.toString());

			ReserveNowParams reserveNowParams = new ReserveNowParams();
			reserveNowParams.setChargePointSelectList(
					customChargePointHelperService.getchargePointSelectList(reservationRequest.getCp()));
			reserveNowParams.setConnectorId(reservationRequest.getConnector());
			reserveNowParams.setExpiry(LocalDateTime.fromDateFields(reservationRequest.getExpires()));
			reserveNowParams.setIdTag(reservationRequest.getIdentifier());
			int taskId = getClient16().reserveNow(reserveNowParams);

			CommunicationTask r = customChargePointHelperService.checkTaskFinished(taskId);
			ReservationResponse reservationResponse = populateReservationResponse(r, reservationRequest.getCp());
			log.info("Response: {} ", reservationResponse);
			return new ResponseEntity<>(reservationResponse, HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	public RemoteStartResponse populateRemoteStartTransaction(CommunicationTask r, ConnectorUuid connectorUuid,
			String idTag) {
		RemoteStartResponse response = new RemoteStartResponse();
		RequestResult reqResult = (RequestResult) r.getResultMap().get(connectorUuid.getChargeBoxId());
		if (reqResult != null && reqResult.getResponse() != null) {
			if (reqResult.getResponse().equals("Accepted")) {
				response.setStatus("ok");
				// extract transactionId from database
				List<Integer> transactionList = customTransactionService
						.getActiveTransactionIds(connectorUuid.getChargeBoxId(), connectorUuid.getConnectorId(), idTag);
				if (!transactionList.isEmpty()) {
					Collections.sort(transactionList);
					response.setTransaction(transactionList.get(transactionList.size() - 1));
				} else {
					throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_TRANSACTION_STARTED,
							HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else {
				response.setStatus("ko");
			}
		} else {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_RESPONSE_FROM_CB,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public RemoteStopResponse populateRemoteStopTransaction(CommunicationTask r, ConnectorUuid connectorUuid) {
		RemoteStopResponse response = new RemoteStopResponse();
		RequestResult reqResult = (RequestResult) r.getResultMap().get(connectorUuid.getChargeBoxId());
		if (reqResult != null && reqResult.getResponse() != null) {
			if (reqResult.getResponse().equals("Accepted")) {
				response.setStatus("ok");
				// extract transactionId from database
			} else {
				response.setStatus("ko");
			}
		} else {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_RESPONSE_FROM_CB,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public ReservationResponse populateReservationResponse(CommunicationTask r, String cp) {
		ReservationResponse response = new ReservationResponse();
		RequestResult reqResult = (RequestResult) r.getResultMap().get(cp);
		if (reqResult != null && reqResult.getResponse() != null) {
			ReserveNowTask reserveNowTask = (ReserveNowTask) r;
			response.setReservationId(reserveNowTask.getParams().getReservationId());
			response.setStatus(reqResult.getResponse());
		} else {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_NO_RESPONSE_FROM_CB,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	public PingResponse populatePingResponse(String ipAddress) {
		if( ipAddress == null || ipAddress.isEmpty()) {
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_IP_ADDRESS_EMPTY, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		PingResponse pingResponse = new PingResponse();
		long sentMillisIpAdress = System.currentTimeMillis();
		try {
			InetAddress inet = InetAddress.getByName(ipAddress);
			if (inet.isReachable(5000)) {
				pingResponse.setStatus("OK");
			} else {
				pingResponse.setStatus("Unreachable");
			}
		} catch (IOException e) {
			pingResponse.setStatus("Unreachable");
		}
		pingResponse.setLatency(String.valueOf(System.currentTimeMillis() - sentMillisIpAdress));
		return pingResponse;
	}

	public void checkAuthentication(String chargeBoxId,String idTag) {
		IdTagInfo idTagInfo = ocppTagService.getIdTagInfo(idTag, true);
		if(idTagInfo == null) {
			log.warn("idTag empty");
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_MISSING,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		OcppTagDetailRecord ocppTagDetail = customOcppTagService.getTagDetail(idTag);
		if(ocppTagDetail == null) {
			log.warn("idTag details empty for idTag {}",idTag);;
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_INVALID,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		ChargeBoxSiteRecord chargeBoxSiteRecord =  customChargePointHelperService.getChargeBoxSite(chargeBoxId);
		if(chargeBoxSiteRecord == null ) {
			log.warn("Site not found for chargebox {}",chargeBoxId);
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_SITE_NOT_FOUND,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(chargeBoxSiteRecord.getAuthorizeAll()!=null && chargeBoxSiteRecord.getAuthorizeAll()) {
			log.info("Authorize all in chargebox {}",chargeBoxId);
			return;
		}
		
		if(!ocppTagDetail.getIsActive()) {
			log.warn("idTag tag {} is inactive ",idTag);
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_BLOCKED,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(idTagInfo.getExpiryDate() != null && new DateTime().isAfter(idTagInfo.getExpiryDate())) {
			log.warn("idTag tag {} is expired",idTag);
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_EXPIRED,
						HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(idTagInfo.getStatus().equals(AuthorizationStatus.CONCURRENT_TX)) {
			log.warn("idTag tag {} is in concurrentTx",idTag);
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_CONCURRENTTX,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(chargeBoxSiteRecord.getSiteTypeId() == null) {
			log.warn("Site Type not found for chargebox {}",chargeBoxId);
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_SITE_TYPE_NOT_FOUND,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (chargeBoxSiteRecord.getSiteTypeId().equals(1) && !ocppTagDetail.getAllowPublicCharging()) {
			log.warn("Public charging not allowed for chargebox {} and idTag {}",chargeBoxId,idTag);
			throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_INVALID,
					HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			if (chargeBoxSiteRecord.getCustomerId() != null && ocppTagDetail.getCustomerId() != null
					&& !chargeBoxSiteRecord.getCustomerId().equals(ocppTagDetail.getCustomerId())
					&& chargeBoxSiteRecord.getTagGroupId() != null && ocppTagDetail.getTagGroupId() != null
					&& !chargeBoxSiteRecord.getTagGroupId().equals(ocppTagDetail.getTagGroupId())) {
				log.warn("Public charging not allowed for chargebox {} and idTag {}",chargeBoxId,idTag);
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_ID_TAG_INVALID,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	
}
