package com.gnrgy.ocppserver.web.dto.request;

import java.util.List;

import lombok.Data;

@Data
public class GetConfigurationRequest {

	String chargeBoxId;
	List<String> key;
}