package com.gnrgy.ocppserver.web.dto.response;

import lombok.Data;

@Data
public class RemoteStartResponse {
	
	private String status;
	private Integer transaction;
	
}
