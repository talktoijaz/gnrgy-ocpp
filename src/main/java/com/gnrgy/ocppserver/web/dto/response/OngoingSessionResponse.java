package com.gnrgy.ocppserver.web.dto.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class OngoingSessionResponse {

	Integer transactionId;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	Date startTime;
	String idTag;
	OngoingSessionChargerResponse charger;
	
}
