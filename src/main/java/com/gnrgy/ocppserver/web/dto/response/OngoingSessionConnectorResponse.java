package com.gnrgy.ocppserver.web.dto.response;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OngoingSessionConnectorResponse {

	String uuid;
	String connectorType;
	String connectorPower;
	Integer maxPower;
	String connectorLabel;
	
	public OngoingSessionConnectorResponse(String uuid, String connectorType, BigDecimal maxPower,
			String connectorLabel) {
		super();
		this.uuid = uuid;
		this.connectorType = connectorType;
		
		if(maxPower != null) {
			this.maxPower = maxPower.multiply(new BigDecimal(1000)).intValue();
			this.connectorPower = String.valueOf(maxPower.stripTrailingZeros()) + "kW";	
		}
		this.connectorLabel = connectorLabel;
	}
	
	
}
