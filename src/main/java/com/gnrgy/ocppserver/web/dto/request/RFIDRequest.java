package com.gnrgy.ocppserver.web.dto.request;

import java.util.Date;

import lombok.Data;

@Data
public class RFIDRequest {
	
	   private Integer maxActiveTransactionCount;
	   private Boolean allowPublicCharging;
	   private Integer custId;
	   private Date expiresAt;
	   private Boolean isActive;
	   private String label;
	   private String parentId;
	   private Integer idTagGroup;
	   private String uid;   
}
