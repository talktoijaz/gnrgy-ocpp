package com.gnrgy.ocppserver.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.web.util.Constant;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PortalAuthenticationInterceptor implements HandlerInterceptor {

	@Value("${gnrgy.portal.authentication.access.token}")
	String accessToken;

	@Value("${gnrgy.portal.authentication.enable:false}")
	String authenticationUrlEnable;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.debug("Request: {}", request.getRequestURL());
		log.debug("AuthenticationUrlEnable: {}", authenticationUrlEnable);
		
		if (authenticationUrlEnable.equalsIgnoreCase("true")) {
			String requestAccessToken = request.getHeader("AccessToken");

			if (requestAccessToken == null) {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_MISSING,HttpStatus.FORBIDDEN);
			}
			
			if(!accessToken.equals(requestAccessToken)) {
				throw new GnrgyOcppserverException(Constant.ERROR_CODE_TOKEN_INVALID,HttpStatus.FORBIDDEN);
			}
		}
		return true;
	}
}