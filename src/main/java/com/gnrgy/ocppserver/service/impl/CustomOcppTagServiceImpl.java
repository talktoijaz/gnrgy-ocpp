package com.gnrgy.ocppserver.service.impl;

import java.util.List;
import java.util.function.Supplier;

import org.jetbrains.annotations.Nullable;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.gnrgy.ocppserver.exception.GnrgyOcppserverException;
import com.gnrgy.ocppserver.repository.CustomChargePointRepository;
import com.gnrgy.ocppserver.repository.CustomOcppTagRepository;
import com.gnrgy.ocppserver.repository.CustomTransactionRepository;
import com.gnrgy.ocppserver.service.CustomOcppTagService;
import com.gnrgy.ocppserver.web.dto.request.RFIDRequest;
import com.gnrgy.ocppserver.web.dto.response.RFIDResponse;
import com.gnrgy.ocppserver.web.util.Constant;

import de.rwth.idsg.steve.service.OcppTagService;
import jooq.steve.db.tables.records.ChargeBoxSiteRecord;
import jooq.steve.db.tables.records.OcppTagDetailRecord;
import lombok.extern.slf4j.Slf4j;
import ocpp.cs._2015._10.AuthorizationStatus;
import ocpp.cs._2015._10.IdTagInfo;

@Slf4j
@Service
public class CustomOcppTagServiceImpl implements CustomOcppTagService{
	
	@Autowired CustomOcppTagRepository customOcppTagRepository;
	
	@Autowired CustomTransactionRepository customTransactionRepository;
	
	@Autowired OcppTagService ocppTagService;
	
	@Autowired CustomChargePointRepository customChargePointRepository;
	
	public List<RFIDResponse> updateOcppTag(List<RFIDRequest> rfidRequestList){
		return customOcppTagRepository.updateOcppTag(rfidRequestList);
	}
	
	public OcppTagDetailRecord getTagDetail(String idTag) {
		return customOcppTagRepository.getTagDetail(idTag);
	}
	
    @Override
    @Nullable
    public IdTagInfo getIdTagInfo(@Nullable String idTag, boolean isStartTransactionReqContext,
                                  Supplier<IdTagInfo> supplierWhenException,String chargeBoxIdentity) {
        try {
            return getIdTagInfo(idTag, isStartTransactionReqContext,chargeBoxIdentity);
        } catch (Exception e) {
            log.error("Exception occurred", e);
            return supplierWhenException.get();
        }
    }
    
    @Override
    @Nullable
    public IdTagInfo getIdTagInfo(@Nullable String idTag, boolean isStartTransactionReqContext,String chargeBoxId) {
    	IdTagInfo idTagInfo = ocppTagService.getIdTagInfo(idTag, isStartTransactionReqContext); 
    	
         if(idTagInfo == null) {
 			log.warn("idTag empty");
 			return new IdTagInfo().withStatus(AuthorizationStatus.INVALID);
 		}
 		
 		OcppTagDetailRecord ocppTagDetail = getTagDetail(idTag);
 		if(ocppTagDetail == null) {
 			log.warn("idTag details empty for idTag {}",idTag);
 			idTagInfo.setStatus(AuthorizationStatus.INVALID);
 			return idTagInfo;
 		}
 		
 		ChargeBoxSiteRecord chargeBoxSiteRecord =  customChargePointRepository.getChargeBoxSite(chargeBoxId);
 		if(chargeBoxSiteRecord == null ) {
 			log.warn("Site not found for chargebox {}",chargeBoxId);
 			idTagInfo.setStatus(AuthorizationStatus.INVALID);
 			return idTagInfo;
 		}
 		
 		if(chargeBoxSiteRecord.getAuthorizeAll()!=null && chargeBoxSiteRecord.getAuthorizeAll()) {
 			log.info("Authorize all in chargebox {}",chargeBoxId);
 			idTagInfo.setStatus(AuthorizationStatus.ACCEPTED);
 			return idTagInfo;
 		}
 		
 		if(!ocppTagDetail.getIsActive()) {
 			log.warn("idTag tag {} is inactive ",idTag);
 			idTagInfo.setStatus(AuthorizationStatus.INVALID);
 			return idTagInfo;
 		}
 		
 		if(idTagInfo.getStatus().equals(AuthorizationStatus.EXPIRED)) {
 			log.warn("idTag tag {} is expired",idTag);
 			return idTagInfo;
 		}
 		
 		if(idTagInfo.getStatus().equals(AuthorizationStatus.CONCURRENT_TX)) {
            log.warn("idTag tag {} is in concurrentTx",idTag);
 			return idTagInfo;
 		}
 		
 		if(chargeBoxSiteRecord.getSiteTypeId() == null) {
 			log.warn("Site Type not found for chargebox {}",chargeBoxId);
 			idTagInfo.setStatus(AuthorizationStatus.INVALID);
 			return idTagInfo;
 		}
 		
 		if (chargeBoxSiteRecord.getSiteTypeId().equals(1) && !ocppTagDetail.getAllowPublicCharging()) {
 			log.warn("Public charging not allowed for chargebox {} and idTag {}",chargeBoxId,idTag);
 			idTagInfo.setStatus(AuthorizationStatus.INVALID);
 			return idTagInfo;
 		} else {
 			if (chargeBoxSiteRecord.getCustomerId() != null && ocppTagDetail.getCustomerId() != null
 					&& !chargeBoxSiteRecord.getCustomerId().equals(ocppTagDetail.getCustomerId())
 					&& chargeBoxSiteRecord.getTagGroupId() != null && ocppTagDetail.getTagGroupId() != null
 					&& !chargeBoxSiteRecord.getTagGroupId().equals(ocppTagDetail.getTagGroupId())) {
 				log.warn("Public charging not allowed for chargebox {} and idTag {}",chargeBoxId,idTag);
 				idTagInfo.setStatus(AuthorizationStatus.INVALID);
 	 			return idTagInfo;
 			}
 		}
 		return idTagInfo;
    }
}
