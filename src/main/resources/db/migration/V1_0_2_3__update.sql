ALTER TABLE `connector_types`
    DROP COLUMN `max_power`;

ALTER TABLE `connectors_detail`
    ADD COLUMN `tariff_type_id` int(11) unsigned AFTER `connector_type_id`;
    
ALTER TABLE `connectors_detail`
    ADD COLUMN `max_power` decimal(5,2) unsigned AFTER `tariff_type_id`;

CREATE TABLE `charge_box_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(20)  NOT NULL,
  `model` varchar(20)  NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_At` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_model_vendor` (`model`,`vendor`)
)  ENGINE=InnoDB CHARSET=latin1;

ALTER TABLE `charge_box_site`
    ADD COLUMN `model_id` int(11) AFTER `site_type_id`;
    
 CREATE TABLE `ocpp_action_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_At` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`action`)
) ENGINE=InnoDB CHARSET=latin1;

insert into ocpp_action_types values
(1,"Heartbeat",now()),
(2,"StatusNotification",now()),
(3,"StartTransaction",now()),
(4,"StopTransaction",now()),
(5,"MeterValues",now()),
(6,"Authorize",now()),
(7,"BootNotification",now()),
(8,"Auth",now()),
(9,"ChangeAvailability",now()),
(10,"ClearCache",now()),
(11,"FirmwareStatusNotification",now()),
(12,"GetLocalListVersion",now()),
(13,"Meter",now()),
(14,"RemoteStartTransaction",now()),
(15,"Reset",now()),
(16,"SendLocalList",now()),
(17,"UpdateFirmware",now()),
(18,"RemoteStopTransaction",now()),
(19,"DiagnosticsStatusNotification",now()),
(20,"ChangeConfiguration",now()),
(21,"CancelReservation",now()),
(22,"ClearChargingProfile",now()),
(23,"DataTransfer",now()),
(24,"GetCompositeSchedule",now()),
(25,"GetConfiguration",now()),
(26,"GetDiagnostics",now()),
(28,"ReserveNow",now()),
(29,"SetChargingProfile",now()),
(30,"TriggerMessage",now()),
(31,"UnlockConnector",now()),
(32,"RemoteStart",now());

CREATE TABLE `ocpp_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `charge_box_id` int(11) DEFAULT NULL,
  `ip_address` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_type_id` int(11) DEFAULT NULL,
  `request` blob DEFAULT NULL,
  `response` blob DEFAULT NULL,
  `status_code` smallint(5) DEFAULT NULL,
  `response_time` mediumint(9) DEFAULT NULL,
  `created_At` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx_createdAt_statusCode_responseTime` (`created_At`,`status_code`,`response_time`),
  KEY `idx_createdAt_cbId_actionType` (`created_At`,`charge_box_id`,`action_type_id`)
) ENGINE=InnoDB CHARSET=latin1;



    