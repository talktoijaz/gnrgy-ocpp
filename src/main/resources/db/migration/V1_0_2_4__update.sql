ALTER TABLE `ocpp_tag_detail`
    MODIFY COLUMN `tag_group_id` int(11);
    
ALTER TABLE `connectors_detail`
    MODIFY COLUMN `connector_type_id` int(11);
    
ALTER TABLE `charge_box_site`
    DROP COLUMN `model_id`;    
    
DROP TABLE `charge_box_models`;